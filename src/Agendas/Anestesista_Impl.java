/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agendas;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author a1906453
 */
public class Anestesista_Impl extends UnicastRemoteObject implements InterfaceAnest{

    private List<Horarios> horariosAnest;
    private Boolean Saleiro;
    private FileWriter arq;
    private PrintWriter gravarArq;
    private Integer ordem;
    private String destinoLog = "/home/todos/alunos/ct/a1906453/Área de Trabalho/";//Alterar aqui onde o Log é salvo
    
    
    public Anestesista_Impl() throws RemoteException, IOException{
        this.horariosAnest = new ArrayList<Horarios>();
        this.Saleiro = true;
        this.arq = new FileWriter(destinoLog+"logAnestINICIO.txt");
        this.gravarArq = new PrintWriter(arq);
        this.ordem = 0;
    }

    @Override
    public void Efetivar_Transacao(Integer horaInicio, Integer horaFim, Integer dia, Integer mes, Integer ano) throws RemoteException {
        Horarios novoHorario = new Horarios(horaInicio,horaFim,dia,mes,ano);
        horariosAnest.add(novoHorario);
        Saleiro = true;
        gravarArq.printf("Transação efetivada;");
        try {
            arq.close();
        } catch (IOException ex) {
            Logger.getLogger(Anestesista_Impl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Abortar() throws RemoteException {
        Saleiro = true;
        gravarArq.printf("Transação abortada;");
        try {
            arq.close();
        } catch (IOException ex) {
            Logger.getLogger(Anestesista_Impl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Boolean Obter_Decisao(Integer horaInicio, Integer horaFim, Integer dia, Integer mes, Integer ano) throws RemoteException {
        if(Saleiro==false){
            while(Saleiro==false){
                //wait
            }
        }
        Saleiro = false;
        try {
            arq = new FileWriter(destinoLog+"Log_Anest"+ordem.toString()+".txt");
            ordem = ordem + 1;
            gravarArq = new PrintWriter(arq);
        } catch (IOException ex) {
            Logger.getLogger(Anestesista_Impl.class.getName()).log(Level.SEVERE, null, ex);
        }
        gravarArq.printf("Transação iniciada;");
        Boolean decisao = true;
        for(int i=0;i<horariosAnest.size();i++){
            Horarios hora=horariosAnest.get(i);
            if(hora.dia.equals(dia) && hora.mes.equals(mes) && hora.ano.equals(ano)){
                if((hora.horarioInicio<=horaFim && hora.horarioInicio>=horaInicio)||(hora.horarioTermino<=horaFim && hora.horarioTermino>=horaInicio)||
                        (hora.horarioInicio<=horaInicio && hora.horarioTermino>=horaFim)){
                    decisao = false;
                    gravarArq.printf("Transição falha;");
                }
            }
        }
        return decisao;
    }
    
}
