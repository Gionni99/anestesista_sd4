/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agendas;

import java.rmi.*;

/**
 *
 * @author a1906453
 */
public interface InterfaceAnest extends Remote{
    //Anestesista
    public void Efetivar_Transacao(Integer horaInicio, Integer horaFim, Integer dia, Integer mes, Integer ano) throws RemoteException;
    public void Abortar() throws RemoteException;
    public Boolean Obter_Decisao(Integer horaInicio,Integer horaFim,Integer dia,Integer mes,Integer ano) throws RemoteException;
    
    
}
