/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agendas;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author a1906453
 */
public class Anestesista {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws RemoteException, AlreadyBoundException, IOException {
        Anestesista_Impl anastasia = new Anestesista_Impl();
        Registry registro = LocateRegistry.createRegistry(1099);
        registro.bind("Anestesista", anastasia);
    }
    
}
