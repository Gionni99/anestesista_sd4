/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agendas;

/**
 *
 * @author Giovanni Bandeira
 */
public class Horarios {

    public Integer horarioInicio;
    public Integer horarioTermino;
    public Integer dia;
    public Integer mes;
    public Integer ano;
    
    public Horarios(Integer horaInicio,Integer horaFim,Integer dia,Integer mes,Integer ano) {
        this.horarioInicio=horaInicio;
        this.horarioTermino=horaFim;
        this.dia=dia;
        this.mes=mes;
        this.ano=ano;
    }
    
}
